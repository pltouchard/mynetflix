package com.zareapp.beans.fr;

import java.util.List;

public class CompteUtilisateur {
	public String utilisateur;
	public String motDePasse;
	
	private List<String> roles;

	public CompteUtilisateur() {
		
	}

	public CompteUtilisateur(String utilisateur, String motDePasse, String... roles) {
		super();
		this.utilisateur = utilisateur;
		this.motDePasse = motDePasse;
		
		if(roles != null) {
			for(String r : roles) {
				this.roles.add(r);
			}
		}
	}

	public String getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(String utilisateur) {
		this.utilisateur = utilisateur;
	}

	public String getMotDePasse() {
		return motDePasse;
	}

	public void setMotDePasse(String motDePasse) {
		this.motDePasse = motDePasse;
	}

	public List<String> getRoles() {
		return roles;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}
	
	
	
	
	
}
