package com.zareapp.DAO.fr;

import java.sql.Connection;
import java.util.HashMap;

public abstract class SimpleDAO<T> {
	protected Connection connect = null;
	
	public SimpleDAO(Connection conn) {
		this.connect = conn;
	}
	
	public abstract T find(int id);
	
	public abstract HashMap<Integer, T> findAll();

}
