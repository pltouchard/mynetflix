package com.zareapp.DAO.fr;

import java.sql.Connection;
import java.util.HashMap;

public abstract class DAO<T> {
	protected Connection connect = null;
	
	public DAO(Connection conn) {
		this.connect = conn;
	}
	
	public abstract boolean create(T obj);
	
	public abstract boolean delete(T obj);
	
	public abstract boolean update(T obj);
	
	public abstract T find(int id);
	
	public abstract T find(int idSaison, int idEpisode);
	
	public abstract HashMap<Integer, T> findAll();
	
	public abstract HashMap<Integer, T> findAll(int id);
}
