package com.zareapp.DAO.fr;
//package g2.DAO.fr;
//
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.sql.Statement;
//import java.util.HashMap;
//
//import g2.objetsMetier.fr.Serie;
//
//public class SerieDAO extends DAO<Serie> {
//
//	public SerieDAO(Connection conn) {
//		super(conn);
//	}
//
//	@Override
//	public boolean create(Serie serie) {
//
//		try {
//			PreparedStatement createStatement = connect.prepareStatement(
//					"INSERT INTO serie (nom, nomOriginal, anneeparution, synopsys, idstatut, idpaysorigine) VALUES (?,?,?,?,?,?)");
//
//			createStatement.setString(1, serie.getNom());
//			createStatement.setString(2, serie.getNomOriginal());
//			createStatement.setInt(3, serie.getAnneeParution());
//			createStatement.setString(4, serie.getSynopsys());
//			createStatement.setInt(5, serie.getStatut().getId());
//			createStatement.setInt(6, serie.getPaysOrigine().getId());
//
//			createStatement.executeUpdate();
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return false;
//	}
//
//	@Override
//	public boolean delete(Serie serie) {
//		try {
//			PreparedStatement delStatement = connect.prepareStatement("DELETE FROM serie WHERE id = ?");
//
//			delStatement.setInt(1, serie.getId());
//
//			delStatement.executeUpdate();
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return false;
//	}
//
//	@Override
//	public boolean update(Serie serie) {
//		try {
//			PreparedStatement updateStatement = connect.prepareStatement(
//					"UPDATE serie SET nom = ?, nomOriginal = ?, anneeParution = ?, synopsys = ?, idStatut = ?, idPaysOrigine = ? WHERE id = ?");
//
//					
//					updateStatement.setString(1, serie.getNom());
//					updateStatement.setString(2, serie.getNomOriginal());
//					updateStatement.setInt(3, serie.getAnneeParution());
//					updateStatement.setString(4, serie.getSynopsys());
//					updateStatement.setInt(5, serie.getStatut().getId());
//					updateStatement.setInt(6, serie.getPaysOrigine().getId());
//					updateStatement.setInt(7, serie.getId());
//
//			updateStatement.executeUpdate();
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return false;
//	}
//
//	@Override
//	public Serie find(int id) {
//		Serie serie = new Serie();
//
//		ResultSet result;
//		try {
//			PreparedStatement findStatement = connect.prepareStatement("SELECT * FROM serie WHERE id = ?");
//			findStatement.setInt(1, id);
//			result = findStatement.executeQuery();
//			if (result.first())
//				serie = new Serie(
//				result.getInt("id"),
//				result.getString("nom"),
//				result.getString("nomOriginal"),
//				result.getInt("anneeParution"),
//				result.getString("synopsys"),
//				result.getInt("idStatut"),
//				result.getInt("idPaysOrigine"));
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//
//		return serie;
//
//	}
//
//	@Override
//	public HashMap<Integer, Serie> findAll() {
//		HashMap<Integer, Serie> listeSerie = new HashMap<Integer, Serie>();
//
//		try {
//			Statement statement = connect.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
//			String sql = "SELECT * FROM serie ORDER BY nom ASC";
//			ResultSet resultat = statement.executeQuery(sql);
//
//			while (resultat.next()) {
//				int id = resultat.getInt("id");
//				String nom = resultat.getString("nom");
//				String nomOriginal = resultat.getString("nomOriginal");
//				int anneeParution = resultat.getInt("anneeParution");
//				String synopsys = resultat.getString("synopsys");
//				int idStatut = resultat.getInt("idStatut");
//				int idPaysOrigine = resultat.getInt("idPaysOrigine");
//				listeSerie.put(id, new Serie(id, nom, nomOriginal, anneeParution, synopsys, idStatut, idPaysOrigine));
//			}
//
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//		return listeSerie;
//	}
//
//	@Override
//	public HashMap<Integer, Serie> findAll(int id) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public Serie find(int idSaison, int idEpisode) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//}
