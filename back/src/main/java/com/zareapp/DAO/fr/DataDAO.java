package com.zareapp.DAO.fr;

import java.util.HashMap;
import java.util.Map;

import com.zareapp.beans.fr.CompteUtilisateur;
import com.zareapp.utilitaires.fr.SecurityConfig;

public class DataDAO {
	private static final Map<String, CompteUtilisateur> mapUsers = new HashMap<String, CompteUtilisateur>();
	 
	   static {
	      initUsers();
	   }
	 
	   private static void initUsers() {
	 
	      // This user has a role as EMPLOYEE.
		   CompteUtilisateur emp = new CompteUtilisateur("employee1", "123", SecurityConfig.ROLE_MANAGER);
	 
	      // This user has 2 roles EMPLOYEE and MANAGER.
		   CompteUtilisateur mng = new CompteUtilisateur("manager1", "123", SecurityConfig.ROLE_MANAGER);
	 
	      mapUsers.put(emp.getUtilisateur(), emp);
	      mapUsers.put(mng.getUtilisateur(), mng);
	   }
	 
	   // Find a User by userName and password.
	   public static CompteUtilisateur findUser(String utilisateur, String motDePasse) {
		   CompteUtilisateur u = mapUsers.get(utilisateur);
	      if (u != null && u.getMotDePasse().equals(motDePasse)) {
	    	 
	         return u;
	      }
	      return null;
	   }
}
	