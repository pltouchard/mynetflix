package com.zareapp.DAO.fr;
//package g2.DAO.fr;
//
//import java.sql.Connection;
//import java.sql.Date;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.util.HashMap;
//import java.util.Map;
//
//import g2.objetsMetier.fr.Episode;
//import g2.objetsMetier.fr.Personne;
//
//public class EpisodeDAO extends DAO<Episode> {
//
//	public EpisodeDAO(Connection conn) {
//		super(conn);
//	}
//
//	@Override
//	public boolean create(Episode episode) {
//
//		try {
//			PreparedStatement createStatement = connect.prepareStatement(
//					"INSERT INTO episode (numero, titre, titreoriginal, duree, resume, dateRealisation, date_premiere_diffusion, idPublic, idStatut, idSaison) VALUES (?,?,?,?,?,?,?,?,?,?)");
//					
//			createStatement.setInt(1, episode.getNumero());
//			createStatement.setString(2, episode.getTitre());
//			createStatement.setString(3, episode.getTitreOriginal());
//			createStatement.setInt(4, episode.getDuree());
//			createStatement.setString(5, episode.getResume());
//			createStatement.setDate(6, episode.getDateRealisation());
//			createStatement.setDate(7, episode.getDatePremiereDiffusion());
//            createStatement.setInt(8, episode.getPub().getId());
//            createStatement.setInt(9, episode.getStatut().getId());
//            createStatement.setInt(10, episode.getIdSaison());
//
//			createStatement.executeUpdate();
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return false;
//	}
//
//	@Override
//	public boolean delete(Episode episode) {
//		try {
//			PreparedStatement delStatement = connect.prepareStatement("DELETE FROM episode WHERE id = ?");
//
//			delStatement.setInt(1, episode.getId());
//
//			delStatement.executeUpdate();
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return false;
//	}
//
//	@Override
//	public boolean update(Episode episode) {
//		try {
//			PreparedStatement updateStatement = connect.prepareStatement(
//					"UPDATE episode SET numero = ?, titre = ?, titreoriginal = ?, duree = ?, resume = ?, dateRealisation = ?, date_premiere_diffusion = ?, idPublic = ?, idStatut = ?, idSaison = ? WHERE id = ?");
//
//
//			updateStatement.setInt(1, episode.getNumero());
//			updateStatement.setString(2, episode.getTitre());
//			updateStatement.setString(3, episode.getTitreOriginal());
//			updateStatement.setInt(4, episode.getDuree());
//			updateStatement.setString(5, episode.getResume());
//			updateStatement.setDate(6, episode.getDateRealisation());
//			updateStatement.setDate(7, episode.getDatePremiereDiffusion());
//            updateStatement.setInt(8, episode.getPub().getId());
//            updateStatement.setInt(9, episode.getStatut().getId());
//            updateStatement.setInt(10, episode.getIdSaison());
//			updateStatement.setInt(11, episode.getId());
//
//			updateStatement.executeUpdate();
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return false;
//	}
//
//	@Override
//	public Episode find(int id) {
//		Episode episode = new Episode();
//
//		ResultSet result;
//		try {
//			PreparedStatement findStatement = connect.prepareStatement("SELECT * FROM episode WHERE id = ?");
//			findStatement.setInt(1, id);
//			result = findStatement.executeQuery();
//			if (result.first())
//            episode = new Episode(
//            	result.getInt("id"),
//				result.getInt("numero"),
//				result.getString("titre"),
//				result.getString("titreoriginal"),
//				result.getInt("duree"),
//				result.getString("resume"),
//				result.getDate("daterealisation"),
//				result.getDate("date_premiere_Diffusion"),
//				result.getInt("idpublic"),
//				result.getInt("idstatut"),
//				result.getInt("idsaison"));
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//
//		return episode;
//	}
//	
//	@Override
//	public Episode find(int idSaison, int idEpisode) {
//		Episode episode = null;
//
//		ResultSet result;
//		try {
//			PreparedStatement findStatement = connect.prepareStatement("SELECT * FROM episode WHERE idsaison = ? AND numero = ?");
//			findStatement.setInt(1, idSaison);
//			findStatement.setInt(2, idEpisode);
//			result = findStatement.executeQuery();
//			if (result.first()) {
//				episode = new Episode(
//            	result.getInt("id"),
//				result.getInt("numero"),
//				result.getString("titre"),
//				result.getString("titreoriginal"),
//				result.getInt("duree"),
//				result.getString("resume"),
//				result.getDate("daterealisation"),
//				result.getDate("date_premiere_Diffusion"),
//				result.getInt("idpublic"),
//				result.getInt("idstatut"),
//				result.getInt("idsaison"));
//			}
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//
//		return episode;
//
//	}
//
//	@Override
//	public HashMap<Integer, Episode> findAll(int idSaisonRequest) {
//		HashMap<Integer, Episode> listeEpisode = new HashMap<Integer, Episode>();
//
//		try {
//			PreparedStatement findStatement = connect.prepareStatement("SELECT * FROM episode WHERE idsaison = ? order by id");
//			findStatement.setInt(1, idSaisonRequest);
//			ResultSet resultat = findStatement.executeQuery();
//
//			while (resultat.next()) {
//				int id = resultat.getInt("id");
//				int numero = resultat.getInt("numero");
//				String titre = resultat.getString("titre");
//				String titreOriginal = resultat.getString("titreoriginal");
//				int duree = resultat.getInt("duree");
//				String resume = resultat.getString("resume");
//				Date dateRealisation = resultat.getDate("daterealisation");
//				Date datePremiereDiffusion = resultat.getDate("date_premiere_diffusion");
//				int idPublic = resultat.getInt("idPublic");
//				int idStatut = resultat.getInt("idStatut");
//				int idSaison = resultat.getInt("idSaison");
//				listeEpisode.put(id, new Episode(id, numero, titre, titreOriginal, duree, resume, dateRealisation, datePremiereDiffusion, idPublic, idStatut, idSaison));
//			}
//
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//		return listeEpisode;
//	}
//
//	@Override
//	public HashMap<Integer, Episode> findAll() {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//}
