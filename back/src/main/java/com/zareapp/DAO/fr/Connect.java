package com.zareapp.DAO.fr;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connect {
    
    private String url = "jdbc:mariadb://localhost:3306/mynetflix";
    private String user = "root";
    private String passwd = "toor";
    private static Connection connect;
     
    private Connect(){
      try {
        connect = DriverManager.getConnection(url, user, passwd);
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }
     
    //M�thode qui va nous retourner notre instance et la cr�er si elle n'existe pas
     public static Connection getInstance(){
      if(connect == null){
        new Connect();
      }
      return connect;
      
    }   

}

