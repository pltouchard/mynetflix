package com.zareapp.filter.fr;

import java.io.IOException;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zareapp.beans.fr.CompteUtilisateur;
import com.zareapp.request.fr.UserRoleRequestWrapper;
import com.zareapp.utilitaires.fr.AppUtils;
import com.zareapp.utilitaires.fr.SecurityUtils;

@WebFilter("/*")
public class SecurityFilter implements Filter{
	
	public SecurityFilter() {
    }
	
	@Override
    public void destroy() {
    }

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;
        
        String servletPath = request.getServletPath();
        
        // Stocker infos de l'utilisateur dans une session après connexion
        CompteUtilisateur loginedUser = AppUtils.getLoginedUser(request.getSession());
        
        if (servletPath.equals("/login")) {
            chain.doFilter(request, response);
            return;
        }
        HttpServletRequest wrapRequest = request;
 
        if (loginedUser != null) {
            // nom d'utilisateur
            String userName = loginedUser.getUtilisateur();
 
            // Roles
            List<String> roles = loginedUser.getRoles();
 
            // Créer une nouvelle requete avec l'ancienne + le nom d'utilisateur et le role
            wrapRequest = new UserRoleRequestWrapper(userName, roles, request);
        }
 
        // Il faut être identifié pour acccéder aux pages
        if (SecurityUtils.isSecurityPage(request)) {
 
            //Si utilisateur non connecté -> redirection à la page de login
            if (loginedUser == null) {
 
                String requestUri = request.getRequestURI();
 
                // Stocker la page demandée pour y revenir automatiquement une fois connecté
                int redirectId = AppUtils.storeRedirectAfterLoginUrl(request.getSession(), requestUri);
 
                response.sendRedirect(wrapRequest.getContextPath() + "/login?redirectId=" + redirectId);
                return;
            }
 
            //Vérifier que l'utilisateur connecté a bien la permission d'accéder à cette page
            boolean hasPermission = SecurityUtils.hasPermission(wrapRequest);
            if (!hasPermission) {
 
                RequestDispatcher dispatcher //
                        = request.getServletContext().getRequestDispatcher("/WEB-INF/jsp/acces-refuse.jsp");
 
                dispatcher.forward(request, response);
                return;
            }
        }
 
        chain.doFilter(wrapRequest, response);
		
	}
	
	
}
