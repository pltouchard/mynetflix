package com.zareapp.servlet.fr;
//package g2.servlets.fr;
//
//import java.io.IOException;
//import java.sql.Connection;
//import java.sql.SQLException;
//import java.util.HashMap;
//import java.util.Map;
//
//import javax.annotation.Resource;
//import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.sql.DataSource;
//
//import g2.DAO.fr.Connect;
//import g2.DAO.fr.DAO;
//import g2.DAO.fr.EpisodeDAO;
//import g2.DAO.fr.SaisonDAO;
//import g2.DAO.fr.SerieDAO;
//import g2.objetsMetier.fr.Episode;
//import g2.objetsMetier.fr.Saison;
//import g2.objetsMetier.fr.Serie;
//
//@WebServlet(urlPatterns = "/serie", loadOnStartup=1)
//public class SerieControlerServlet extends HttpServlet {
//		@Resource(name = "series")
//		private DataSource dataSource;
//		
//		private static final String VUE_ACCUEIL = "/WEB-INF/jsp/index.jsp";
//		private static final String VUE_SERIE = "/WEB-INF/jsp/serie.jsp";
//		
//		@Override
//		protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {		
//			try (Connection connection = dataSource.getConnection()) {
//				DAO<Serie> serieDAO = new SerieDAO(Connect.getInstance());
//				DAO<Saison> saisonDAO = new SaisonDAO(Connect.getInstance());
//				DAO<Episode> episodeDAO = new EpisodeDAO(Connect.getInstance());
//				
//				String idSerieRequest = req.getParameter("idserie");
//				String action = req.getParameter("action");
//				
//				Serie serieRequested = serieDAO.find(Integer.parseInt(idSerieRequest));
//				HashMap<Integer, Saison> lesSaisons = saisonDAO.findAll(Integer.parseInt(idSerieRequest));
//				
//				req.setAttribute("serieRequested", serieRequested);
//				req.setAttribute("lesSaisons", lesSaisons);
//				
//				if(action.equals("supprimer")) {
//					for(Map.Entry<Integer, Saison> entrySaison : lesSaisons.entrySet()) {
//						HashMap<Integer, Episode> lesEpisodes = episodeDAO.findAll(entrySaison.getKey());
//						for(Map.Entry<Integer, Episode> entry : lesEpisodes.entrySet()) {
//							episodeDAO.delete(entry.getValue());
//						}
//					}
//					for(Map.Entry<Integer, Saison> entry : lesSaisons.entrySet()) {
//						saisonDAO.delete(entry.getValue());
//					}
//					Serie serie = serieDAO.find(Integer.parseInt(idSerieRequest));
//					serieDAO.delete(serie);
//					resp.sendRedirect(req.getContextPath());
//				}else {
//					getServletContext().getRequestDispatcher(VUE_SERIE).forward(req, resp);
//				}
//				
//				
//			    } catch (SQLException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//		}
//}
