package com.zareapp.servlet.fr;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zareapp.DAO.fr.DataDAO;
import com.zareapp.beans.fr.CompteUtilisateur;
import com.zareapp.utilitaires.fr.AppUtils;



@WebServlet("/login")
public class LoginServlet extends HttpServlet{

	 private static final long serialVersionUID = 1L;
	 
	 public LoginServlet() {
        super();
    }
	 
	 @Override
	    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	            throws ServletException, IOException {
	 
	        RequestDispatcher dispatcher //
	                = this.getServletContext().getRequestDispatcher("/WEB-INF/jsp/login.jsp");
	 
	        dispatcher.forward(request, response);
	    }
	 
	 @Override
	    protected void doPost(HttpServletRequest request, HttpServletResponse response)
	            throws ServletException, IOException {
	 
	        String userName = request.getParameter("utilisateur");
	        String password = request.getParameter("motDePasse");
	        CompteUtilisateur userAccount = DataDAO.findUser(userName, password);
	        
	 
	        if (userAccount == null) {
	            String errorMessage = "Utilisateur ou mot de passe invalide";
	 
	            request.setAttribute("errorMessage", errorMessage);
	 
	            RequestDispatcher dispatcher //
	                    = this.getServletContext().getRequestDispatcher("/WEB-INF/jsp/login.jsp");
	 
	            dispatcher.forward(request, response);
	            return;
	        }
	 
	        AppUtils.storeLoginedUser(request.getSession(), userAccount);
	 
	        // 
	        int redirectId = -1;
	        try {
	            redirectId = Integer.parseInt(request.getParameter("redirectId"));
	        } catch (Exception e) {
	        }
	        String requestUri = AppUtils.getRedirectAfterLoginUrl(request.getSession(), redirectId);
	        if (requestUri != null) {
	            response.sendRedirect(requestUri);
	        } else {
	            // Default after successful login
	            // redirect to /userInfo page
	            response.sendRedirect(request.getContextPath() + "/userInfo");
	        }
	 
	    }
}
