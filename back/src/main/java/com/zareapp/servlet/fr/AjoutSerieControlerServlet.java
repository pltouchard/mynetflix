package com.zareapp.servlet.fr;
//package g2.servlets.fr;
//
//import java.io.IOException;
//import java.sql.Connection;
//import java.sql.Date;
//import java.sql.SQLException;
//import java.util.ArrayList;
//import java.util.HashMap;
//
//import javax.annotation.Resource;
//import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.sql.DataSource;
//
//import g2.DAO.fr.Connect;
//import g2.DAO.fr.DAO;
//import g2.DAO.fr.SerieDAO;
//import g2.DAO.fr.SimplePaysDAO;
//import g2.DAO.fr.SimpleStatutDAO;
//import g2.objetsMetier.fr.Episode;
//import g2.objetsMetier.fr.Pays;
//import g2.objetsMetier.fr.Serie;
//import g2.objetsMetier.fr.Statut;
//import g2.utilitaires.fr.ErreursAjoutsException;
//
//@WebServlet(urlPatterns = "/ajouter-serie", loadOnStartup=1)
//public class AjoutSerieControlerServlet extends HttpServlet {
//		@Resource(name = "series")
//		private DataSource dataSource;
//		
//		private static final String VUE_SERIE = "/WEB-INF/jsp/ajoutSerie.jsp";
//		
//		@Override
//		protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {		
//			try (Connection connection = dataSource.getConnection()) {
//				req.setCharacterEncoding("UTF-8");
//				
//				DAO<Serie> serieDAO = new SerieDAO(Connect.getInstance());
//				SimpleStatutDAO statutDao = new SimpleStatutDAO(Connect.getInstance());
//				HashMap<Integer, Statut> listeStatuts = statutDao.findAll(3);
//				
//				SimplePaysDAO paysDao = new SimplePaysDAO(Connect.getInstance());
//				HashMap<Integer, Pays> listePays = paysDao.findAll();
//				
//				req.setAttribute("listeStatuts", listeStatuts);
//				req.setAttribute("listePays", listePays);
//				String idSerie = req.getParameter("idserie");
//				String action = req.getParameter("action");
//				if(action.equals("modifier")) {
//					Serie serieAModifier = serieDAO.find(Integer.parseInt(idSerie));
//					req.setAttribute("serieAModifier", serieAModifier);
//				}else if(action.equals("supprimer")) {
//					
//				}
//				
//				getServletContext().getRequestDispatcher(VUE_SERIE).forward(req, resp);
//				
//			    } catch (SQLException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//		}
//		
//		@Override
//		protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//			try (Connection connection = dataSource.getConnection()) {
//				ArrayList<String> erreurs = new ArrayList<String>();
//				
//				req.setCharacterEncoding("UTF-8");
//				DAO<Serie> serieDAO = new SerieDAO(Connect.getInstance());
//				
//				SimpleStatutDAO statutDao = new SimpleStatutDAO(Connect.getInstance());
//				HashMap<Integer, Statut> listeStatuts = statutDao.findAll(3);
//				
//				SimplePaysDAO paysDao = new SimplePaysDAO(Connect.getInstance());
//				HashMap<Integer, Pays> listePays = paysDao.findAll();
//				
//				req.setAttribute("listeStatuts", listeStatuts);
//				req.setAttribute("listePays", listePays);
//				
//				String idSerie = req.getParameter("idserie");
//				String action = req.getParameter("action");
//				String nom = req.getParameter("nom");
//				String nomOriginal = req.getParameter("nomOriginal");
//				String anneeParution = req.getParameter("anneeParution");
//				String synopsis = req.getParameter("synopsis");
//				String statutSerie = req.getParameter("statutSerie");
//				String paysOriginal = req.getParameter("paysOrSerie");
//				
//				try {
//					if(nom == "") {
//						erreurs.add("Le nom de la série ne doit pas être vide !");
//					}
//					
//					if(anneeParution == "") {
//						erreurs.add("L'année de parution ne doit pas être vide !");
//					}
//					
//					if(statutSerie == "") {
//						erreurs.add("Le statut de la série n'est pas renseigné !");
//					}
//					
//		/* Comme pour les saisons et les épisodes, prévoir une solution pour éviter de devoir tout resaisir en cas de problème : trouver */			
//					if(erreurs.isEmpty() && action.equals("ajouter")) {
//						serieDAO.create(new Serie(nom, nomOriginal, Integer.parseInt(anneeParution), synopsis, Integer.parseInt(statutSerie), Integer.parseInt(paysOriginal)));
//						resp.sendRedirect(req.getContextPath() + "/ajouter-serie?action=ajouter");
//					}else if(action.equals("modifier")) {
//						Serie serie = new Serie(Integer.parseInt(idSerie), nom, nomOriginal, Integer.parseInt(anneeParution), synopsis, Integer.parseInt(statutSerie), Integer.parseInt(paysOriginal));
//						serieDAO.update(serie);
//						resp.sendRedirect(req.getContextPath());
//						
//		/* à vérifier si ce qui suit fonctionne puisque certains éléments de l'objet peuvent ne pas être renseignés et provoquer des erreurs... */
//					}else if(!erreurs.isEmpty() && action.equals("modifier")){
//						Serie serieAModifier = new Serie(Integer.parseInt(idSerie), nom, nomOriginal, Integer.parseInt(anneeParution), synopsis, Integer.parseInt(statutSerie), Integer.parseInt(paysOriginal));
//						throw new ErreursAjoutsException(erreurs, serieAModifier);
//					}else if(!erreurs.isEmpty()){
//						throw new ErreursAjoutsException(erreurs);
//					}
//					
//				}catch(ErreursAjoutsException e) {
//					e.printStackTrace();
//					ArrayList<String> err = e.getErreurs();
////					Serie serieAModifier = e.getSerie();
//					req.setAttribute("err", err);
////					req.setAttribute("serieAModifier", serieAModifier);
//					getServletContext().getRequestDispatcher(VUE_SERIE).forward(req, resp);
//				}
//				//getServletContext().getRequestDispatcher(VUE_SERIE).forward(req, resp);
//				
//			    } catch (SQLException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//		}
//}
