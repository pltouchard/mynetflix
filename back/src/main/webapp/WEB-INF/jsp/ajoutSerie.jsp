<%@include file="header.jsp" %>
<%@page pageEncoding="UTF-8" isErrorPage="true" contentType="text/html" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<h1>Ajouter une série</h1>
<div class="bloc-erreurs">
	<c:forEach items="${err}" var="erreur">
		<p class="erreur"> <c:out value="${erreur}"></c:out></p>
	</c:forEach>
</div>
<form action="" method="post" class="ajout-form">

  <div class="field-form text-form">
    <label for="nom">Nom de la série: </label>
    <input type="text" name="nom" id="name" value="${serieAModifier.nom }" required>
  </div>
  
  <div class="field-form text-form">
    <label for=nom-original>Nom original :</label>
    <input type="text" name="nomOriginal" id="nom-original" value="${serieAModifier.nomOriginal}">
  </div>

  <div class="field-form num-form">
    <label for="annee-parution">Année de parution : </label>
    <input type="number" name="anneeParution" id="annee-parution" value="${serieAModifier.anneeParution }" required>
  </div>

  <div class="field-form text-form">
  	<label for="synopsys">Résumé : </label>
    <textarea name="synopsis" placeholder="Résumé de la série..." rows="10">${serieAModifier.synopsys }</textarea>
  </div>
  
  <div class="field-form drop-form">
    <select name="statutSerie" required> 
    	<option value="">Statut de la série</option>
    	<c:forEach items="${listeStatuts}" var="statut">
        	<%-- <option value="${statut.value.id}">${statut.value.libelle}</option> --%>
        	<c:choose>
        		<c:when test="${statut.value.id == serieAModifier.statut.id}">
        			<option value="${statut.value.id}" selected>${statut.value.libelle}</option>
        		</c:when>
        		<c:otherwise>
        			<option value="${statut.value.id}">${statut.value.libelle}</option>
        		</c:otherwise>
        	</c:choose>
    	</c:forEach>
    </select>
  </div>
  
  <div class="field-form drop-form">
    <select name="paysOrSerie" required>
    	<option value="">Pays d'origine de la série</option>
    	<c:forEach items="${listePays}" var="pays">
        	<%-- <option value="${pays.value.id}">${pays.value.nom}</option> --%>
        	<c:choose>
        		<c:when test="${pays.value.id == serieAModifier.paysOrigine.id}">
        			<option value="${pays.value.id}" selected>${pays.value.nom}</option>
        		</c:when>
        		<c:otherwise>
        			<option value="${pays.value.id}">${pays.value.nom}</option>
        		</c:otherwise>
        	</c:choose>
    	</c:forEach>
    </select>
  </div>

  <div class="valid-form">
  <c:if test="${param['action'] == 'modifier' }">
  	<button type="submit">Modifier</button>
  	<p><a href="${pageContext.request.contextPath}/serie?action=afficher&idserie=${serieAModifier.id}">Annuler/retourner à la série</a></p>
  </c:if>
  <c:if test="${param['action'] == 'ajouter' }">
  	<button type="submit">Ajouter</button>
  	<p><a href="${pageContext.request.contextPath}">Annuler/retour aux séries</a></p>
  </c:if>  
    
  </div>
</form>

<%@include file="footer.jsp" %>