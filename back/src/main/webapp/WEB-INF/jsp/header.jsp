<%@page pageEncoding="UTF-8" isErrorPage="true" contentType="text/html" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
  <head>
  	<meta charset="UTF-8">
    <title>Cocktail EE</title>
    <script type="text/javascript" src="//cdn.jsdelivr.net/webjars/org.webjars/jquery/3.4.1/jquery.js"></script>
    <script type="text/javascript" src="resources/js/custom-js.js"></script>
    <link rel="stylesheet" type="text/css" href="<c:url value="resources/css/style.css" />" />
  </head>
  <body>
  <nav id="navigation">
  	<a id="logo" href="${pageContext.request.contextPath}/"><img src="<c:url value="resources/img/logo-seul.jpg" />"></a>
  	<a href="${pageContext.request.contextPath}/userInfo">User Info</a>
  	<a href="${pageContext.request.contextPath}/login">Login</a>
	<a href="${pageContext.request.contextPath}/logout">Logout</a>  
	<span style="color:red">[ ${loginedUser.userName} ]</span> 
  </nav>

  <div id="inner-content">
  	<div id="content">