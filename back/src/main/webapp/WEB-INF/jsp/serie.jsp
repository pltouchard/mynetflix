<%@include file="header.jsp" %>
<%@page pageEncoding="UTF-8" isErrorPage="true" contentType="text/html" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
	
	<h1><c:out default="Non renseigné" value="${serieRequested.nom}" /></h1>
	<div class="presentation">
		<p><span class="titre-pres">Nom original : </span><c:out default="Non renseigné" value="${serieRequested.nomOriginal}" /></p>
		<p><span class="titre-pres">Année de parution : </span><c:out default="Non renseigné" value="${serieRequested.anneeParution}" /></p>
		<p><span class="titre-pres">Synopsys : </span><c:out default="Non renseigné" value="${serieRequested.synopsys}" /></p>
		<p><span class="titre-pres">Pays d'origine : </span><c:out default="Non renseigné" value="${serieRequested.paysOrigine.nom}" /></p>
		<p><span class="titre-pres">Statut : </span><c:out default="Non renseigné" value="${serieRequested.statut.libelle}" /> </p>	
	</div>
	
	<div class="saisons-series">
		<h2>Les saisons disponibles :</h2>
		<ul>
			<c:if test="${empty lesSaisons}">
				<li>Aucune saison disponible pour cette série</li>
			</c:if>
			<c:forEach var="saison" items="${lesSaisons}">
				<li><a href="${pageContext.request.contextPath}/saison?action=afficher&idsaison=${saison.key}">Saison ${saison.value.numero}</a></li>
			</c:forEach>
		</ul>
		<div class="actions-serie">
			<a href="${pageContext.request.contextPath}/ajouter-saison?action=ajouter&idserie=${serieRequested.id}">Ajouter une saison</a>
		</div>
	</div>
	<div class="actions actions-serie">
		<p><a href="${pageContext.request.contextPath}/ajouter-serie?action=modifier&idserie=${serieRequested.id}">Modifier la série</a></p>
		<p><a href="${pageContext.request.contextPath}/serie?action=supprimer&idserie=${serieRequested.id}" id="confirmation">Supprimer la série</a></p>
	</div>
	
<%@include file="footer.jsp" %>